/*
 * Taghreed Safaryan
 *  991494905
 *
 */
package p1;

import java.util.List;

/**
 *
 * @author Taghreed
 */
public class ResearchGroup {
    List <Patient> patients;
    public double wait_Time; 
    
    
    public ResearchGroup (List<Patient> patients, double wait_Time){
        this.patients = patients;
        this.wait_Time=wait_Time;
    
    }
     @Override
    public String toString() {
        return   patients + "Wait Time " + "\t" + wait_Time;
    }
    
    
}
