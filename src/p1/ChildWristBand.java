/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

import java.util.List;

/**
 *
 * @author Taghreed
 */
public class ChildWristBand extends WristBand{
    private List<Parent> parents;
    
    public ChildWristBand (String patient_name, Doctor doctor, BarCode code, List<Parent> parents){
        super (patient_name, doctor, code);
        this.parents= parents;
    }
    
    @Override
    public String toString() {
        return super.toString() + " Parents Name "+ parents;
    }
    
}
