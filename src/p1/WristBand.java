/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

import java.util.Date;

/**
 *
 * @author Taghred
 */
public class WristBand {
    private String name;
    private Date dob;
    private Doctor doctor; 
    private BarCode code; 
    
    public WristBand(String name, Doctor doctor, BarCode code){
        this.name=name;
        this.doctor=doctor;
        this.code=code;
    }
    
   @Override
    public String toString(){
    return "Patient Name" + "\t" + name + "\t"  +"Doctor Name" + "\t" + doctor + "\t" + "Bar Code" + "\t" +code;
    } 
}
