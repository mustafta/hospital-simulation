/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

import java.util.List;

/**
 *
 * @author Taghreed
 */
public class AllergyWristBand extends WristBand{
    private List<Medication> med = null;
    
    public AllergyWristBand (String patient_name, Doctor doctor, BarCode code, List<Medication> med){
        super (patient_name, doctor, code);
        this.med= med;
    }
    
    @Override
    public String toString() {
        return super.toString() + " List of Medication "+ "\t" + med;
    }
}
