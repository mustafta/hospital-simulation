/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Taghreed
 */
public class HospitalSim {

    public static void main(String[] args) {
       Doctor doc = new Doctor ("Dr. Smith");
       BarCode code = new BarCode ("SN123");
       Medication med = new Medication ("Advil");
       
       List <Medication> meds = new ArrayList<Medication>();
       meds.add(med);
       
       String patient_name = "Sara Wilson";
       WristBand band= new AllergyWristBand(patient_name, doc, code, meds);
       
       List<WristBand> bands = new ArrayList <WristBand> ();
       bands.add(band);
       
       Patient patient = new Patient (patient_name, bands);
       List<Patient> patients = new ArrayList <Patient> ();
       patients.add(patient);
       
       double time = 0.35;
       
       ResearchGroup search_group = new ResearchGroup (patients, time);
       
       
       System.out.println( search_group);
       
       
    }
    
}
