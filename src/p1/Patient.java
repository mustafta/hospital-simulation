/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

import java.util.List;

/**
 *
 * @author Taghreed
 */
public class Patient extends Person{
    List <WristBand> bands = null;
    
    public Patient(String name, List <WristBand> bands){
    super(name);
    this.bands = bands;
    }
    
    @Override
    public String toString(){
    return "ER Admission   "+bands;
    } 
}