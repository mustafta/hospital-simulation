/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

/**
 *
 * @author Taghreed
 */
public class BarCode {
    private String code;
    
    public BarCode(String code){
    this.code = code;
    }
    
     @Override
    public String toString() {
        return  code;
    }
    
}
