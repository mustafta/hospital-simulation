/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

/**
 *
 * @author Taghreed
 */
public class Person {
    private String name;
    
    
    protected Person(String name){
    this.name = name;
    }
    
    public String getName(){
    return name;
    }
    
   @Override
    public String toString(){
    return  name ;
    } 
    
}
